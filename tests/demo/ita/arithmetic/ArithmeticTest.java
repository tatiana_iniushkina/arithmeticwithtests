package demo.ita.arithmetic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArithmeticTest {

    @Test
    void multiply() {
        int a = 1 + (int) (Math.random() * 100);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a * b, Arithmetic.multiply(a, b));
    }

    @Test
    void multiplyByZero() {
        int a = 1 + (int) (Math.random() * 100);
        int b = 0;
        assertEquals(a * b, Arithmetic.multiply(a, b));
    }

    @Test
    void multiplyNegative() {
        int a = -5;
        int b = -7;
        assertEquals(a * b, Arithmetic.multiply(a, b));
    }

    @Test
    void multiplyZeroByZero() {
        int a = 0;
        int b = 0;
        assertEquals(a * b, Arithmetic.multiply(a, b));
    }
}